/**
 * @file SpaceLib.cpp
 * @author Julián Morúa Vindas
 * @author José López Picado
 * @author Sebastián Blanco 
 * @brief 
 * @version 0.1
 * @date 2018-12-08
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include <SpaceLib.h>

/* ----------------------------------------------------------------------------------- */
SpaceLib::SpaceLib(){
    this->scale = "KB";
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
SpaceLib::SpaceLib(string _scale){
    this->setScale(_scale);
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
SpaceLib::~SpaceLib(){

};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::setScale(string _scale){
    if(!_scale.compare("KB") || !_scale.compare("MB") || !_scale.compare("GB") ){
        this->scale = _scale;
    }else{
        this->scale = "KB";
    }
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::measure(double* _memState)
{
    // se obtiene el pid del proceso;
    pid_t pid= getpid();
    string route = "/proc/" + to_string(pid) + "/status";
    // lectura del archivo status correspondiente al pid,
    string buffer;
    ifstream fs(route.c_str()); 
    if(fs.is_open()){
        while(getline(fs, buffer)){
            vector<string> aux = split(buffer,'\t');
            if(aux[0].compare("VmData:") == 0){
                _memState[1] = stoi(aux[1]);
            }
            if(aux[0].compare("VmStk:") == 0){
                _memState[2]= stoi(aux[1]);
            }
        }
        _memState[0] = _memState[1] + _memState[2];
    }else {
        cout << "El archivo no existe" << endl;
    }

};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
vector<string> SpaceLib::split(string s, char sep){
    vector<string> vec;
    string buffer;
    for(char c : s){
        if(c != sep){
            buffer += c;
        }else{
            if(buffer.compare("") != 0){
                vec.push_back(buffer);
                buffer.clear();
            }
        }
    }
    vec.push_back(buffer);
    return vec;
}
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
double SpaceLib::convert(double _kb, string _scale){
    if(_scale.compare("KB") == 0){
        return _kb;
    }else if(_scale.compare("MB") == 0){
        return _kb/ static_cast<double>(1000);
    }else if(_scale.compare("GB") == 0){
        return _kb/ static_cast<double>(1000000);
    }else{
        return -1;
    }
}
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::write(double _dat, string _route){
    fstream fs;
    fs.open( _route.c_str(), ios::app);
    fs << _dat << endl;
    fs.close();
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::putMemPoint(){
    this->measure(this->memoryPoint);
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::putMemPoint_0(){
    this->measure(this->memoryPoint_0);
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::putMemPoint_1(){
    this->measure(this->memoryPoint_1);
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
double SpaceLib::getMem(string _typeMem){   
    if(_typeMem.compare("T") == 0){
        return this->convert(this->memoryPoint[0],this->scale);
    }else if(_typeMem.compare("D") == 0){
        return this->convert(this->memoryPoint[1],this->scale);
    }else if(_typeMem.compare("S") == 0){
        return this->convert(this->memoryPoint[2],this->scale);
    }
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
double SpaceLib::getDeltaMem(string _typeMem){
    double delta;
    if(_typeMem.compare("T") == 0){
        delta = memoryPoint_1[0] - memoryPoint_0[0];
        return this->convert(delta,this->scale);
    }else if(_typeMem.compare("D") == 0){
        delta = memoryPoint_1[1] - memoryPoint_0[1];
        return this->convert(delta,this->scale);
    }else if(_typeMem.compare("S") == 0){
        delta = memoryPoint_1[2] - memoryPoint_0[2];
        return this->convert(delta,this->scale);
    }
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::writeMem(string _route, string _typeMem){
    double dat = this->getMem(_typeMem);
    this->write(dat,_route);
};
/* ----------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------- */
void SpaceLib::writeDeltaMem(string _route, string _typeMem){
    double dat = this->getDeltaMem(_typeMem);
    this->write(dat,_route);
};
/* ----------------------------------------------------------------------------------- */