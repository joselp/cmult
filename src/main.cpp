
#include <Cmult.h>
//#include <SpaceLib.h>
//#include <TimeLib.h>

using namespace cmult;

int main(int argc, char const *argv[])
{

    // Se genera la matriz A
    Matrix<int> A = random_matrix<int>(2,2);
    A.print();

    // Se genera la matriz B
    Matrix<int> B = random_matrix<int>(8,8);
    B.print();

    //Matrix<int> C = mult<int>(A,B);
    //C.print();

    return 0;
}
