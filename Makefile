
all: build

# Construye el ejecutable principal
.PHONY: build
build:
	mkdir -p build
	@cd  ./build && cmake .. && make cmult

# Construye el ejecutable del test
build_t:
	mkdir -p build
	@cd  ./build && cmake .. && make cmult_test

# Corre el ejecutable principal
run:
	./build/src/cmult

# Corre los tests 
run_t: build_t
	./build/test/cmult_test

# Corre las simulaciones
run_simululation: build

# Limpia el espacio de trabajo
.PHONY: clean
clean:
	rm -rf build
