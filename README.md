# cmult




## Como compilar este proyecto
Ejecute alguno de estos comandos
```
>> make cmult ()
>> make cmult_test
```

## Como ejecutar los test
Primero ejecute:
```
>> make cmult_test
```

Una vez compilado u creado el ejecutable puede ejecutar los siguientes comandos:

1. Correr todos los test:
```
  ./build/test/cmult_test
```
2. Correr solo un test:
```
  ./build/test/cmult_test --gtest_filter=<test_name>
  Ex: ./build/test/cmult_test --gtest_filter=
```

Para habilitar la ejecución en modo depuración ejecute:
```
export TEST_DEBUG=1
```

Para deshabilitarla se pone la variable igual a cero.

## Como correr las simulaciones

### Dependencias
1. build-essential
2. cmake
3. gtest

## Instalar dependencias
```
sudo apt install build-essential

sudo apt install cmake

sudo apt install libgtest-dev
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
```
