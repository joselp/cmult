
#if !defined(CMULT_H)
#define CMULT_H

#define BUFFER_SIZE 6000
#define COLUMNS_WRITTEN 500

typedef unsigned int u_int; 

namespace cmult{

    template <typename Dat>
    class Matrix
    {
    private:

        Dat **data;
        u_int row_size;
        u_int col_size;
    
    public:

        Matrix(u_int row_size, u_int col_size);
        Matrix(const Matrix &A);
        ~Matrix();
        u_int getRowSize();
        u_int getColSize();
        Dat** getData();
        void print();
    };
    
    template <typename Dat>
    static Matrix<Dat> mult(Matrix<Dat> &A, Matrix<Dat> &B);

    template <typename Dat>
    static Matrix<Dat> random_matrix(u_int row_size, u_int col_size);

    //Matrix<Dat> multBlocks(Matrix<Dat> A, Matrix<Dat> B);
    //Matrix<Dat> strassen_mult(Matrix A, Matrix B);
    //Matrix<Dat> paralel_mult(Matrix A, Matrix B);
    //Matrix<Dat> paralel_mult_blocks(Matrix A, Matrix B);
    //Matrix<Dat> paralel_strassen_mult(Matrix A, Matrix B);
    //Matrix<Dat> load_matrix();
    //void save_matric(Matrix<Dat> A);
}

#include <Cmult.tpp>

#endif // CMULT_H
