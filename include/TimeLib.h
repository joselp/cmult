/**
 * @file TimeLib.h
 * @author Julián Morúa Vindas
 * @author José López Picado
 * @author Sebastián Blanco 
 * @brief Archivo cabecera de la clase TimeLib
 * @version 0.1
 * @date 2018-12-08
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#if !defined(TIMELIB__H)
#define TIMELIB__H

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <math.h>       

using namespace std;
using namespace std::chrono;

/**
 * @brief La clase TimeLib tiene como objetivo simplificar
 * la medición de tiempos de ejecución de métodos o funciones,
 * con la posibilidad de escribir los tiempos en un archivo 
 * de texto plano y de escoger la escala temporal, que puede 
 * ser: 
 * ns -> nanosegundos
 * ms -> milisegundos 
 * s  -> segundos
 * m  -> minutos 
 */
class TimeLib
{
  private:
    /**
     * @brief Escala temporal
     * 
     */
    string scale;
    /**
     * @brief Tiempo inicial
     */
    steady_clock::time_point t0;
    /**
     * @brief Tiempo final
     */
    steady_clock::time_point t1;

  public:
    /**
     * @brief Constructor por defecto
     */
    TimeLib();


    /**
     * @brief Constructor sobrecargado, recibe la escala
     * para la escritura de los tiempos
     * 
     * @param scale string con la escala temporal
     */
    TimeLib(string _scale);


    /**
     * @brief Destructor por defecto
     */
    ~TimeLib();


    /**
     * @brief Establece la escala temporal para la escritura
     * de los tiempos.
     * 
     */
    void setScale(string _scale);


    /**
     * @brief Toma la primer medición temporal
     */
    void start();


    /**
     * @brief Toma la segunda medición temporal
     * 
     */
    void stop();


    /**
     * @brief Retorna el tiempo de ejecución de la tarea,
     * método o función.
     * 
     * @param scale escala temporal (ns,us,ms,s,m).
     * @return double tiempo de ejecución.
     */
    double getTime();


    /**
     * @brief Escribe en un archivo el tiempo de ejecución
     * de una tarea, método o función.
     * 
     * @param route ruta del archivo de texto plano.
     * @param scale escala temporal en la que se desea guardar el tiempo.
     */
    void writeTime(string route);
};

#endif // TIMELIB__H
