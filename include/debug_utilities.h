
#if !defined(DEBUG_UTILITIES_H)
#define DEBUG_UTILITIES_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>


/* Get enviroment var */
void get_env_var( const char* var_name,int *var_value );


#endif // DEBUG_UTILITIES_H



