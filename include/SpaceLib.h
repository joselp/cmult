/**
 * @file SpaceLib.h
 * @author Julián Morúa Vindas
 * @author José López Picado
 * @author Sebastián Blanco 
 * @brief Archivo cabecera de la clase SpaceLib
 * @version 0.1
 * @date 2018-12-08
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#if !defined(SPACELIB__H)
#define SPACELIB__H

// bibliotecas para la obtención del pid del proceso.
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

/**
 * @brief La función de la clase SpaceLib es facilitar al programador
 * la tediosa tarea de medir el tamaño en RAM de un programa en tiempo
 * de ejecusión, habilitando la posibilidad de guardar la información 
 * el archivos de texto plano para su posterior análisis. Además la 
 * escala de espacio en bytes puede ser escogida.
 * KB -> kilobytes
 * MB -> megabytes
 * GB -> gigabytes
 */
class SpaceLib
{
private:
    /**
     * @brief Escala de bytes.
     */
    string scale;
    /**
     * @brief Estado de la memoria del proceso en el punto
     * [memTotal, memDynamic, memStatic]
     */
    double memoryPoint[3];
    /**
     * @brief Estado de la memoria del proceso en el punto 0 
     * [memTotal, memDynamic, memStatic]
     */
    double memoryPoint_0[3];
    /**
     * @brief Estado de la memoria del proceso en el punto 1 
     * [memTotal, memDynamic, memStatic].
     */
    double memoryPoint_1[3];
    /**
     * @brief Realiza un split a un string utilizando el separador
     * que le llega por parámetro y retorna un vector con las palabras.
     * 
     * @param s string con la hilera de caracteres.
     * @param sep caracter separador
     * @return vector<string> vector con las palabras del string
     */
    vector<string> split(string _s, char _sep);
    /**
     * @brief Realiza la consulta del archivo status correspondiente
     * al pid del programa y almacena el estado de la memoria total,
     * dinámica y estática en el puntero al arreglo que le llega por 
     * parámetro.
     * @param _memState puntero al arreglo en el que se guardará el estado.
     * @return vector<string> parabras del string _s.
     */
    void measure(double* _memState);
    /**
     * @brief Convierte de kilobytes a megabytes o gigabytes.
     * 
     * @param _kb magnitud en kilobytes que se desea convertir.
     * @param scale escala a la que se desea convertir.
     * @return double conversión
     */
    double convert(double _kb, string scale);
    /**
     * @brief Escribe datos en un fichero.
     * 
     * @param _dat dato a escribir.
     * @param _route ruta donde se ha de escribir.
     */
    void write(double _dat, string _route);
public:
    /**
     * @brief Constructor por defecto.
     */
    SpaceLib();


    /**
     * @brief Constructor modificado, recibe la escala de bytes.
     */
    SpaceLib(string _scale);


    /**
     * @brief Destructor por defecto.
     */
    ~SpaceLib();


    /**
     * @brief Establece la escala en bytes para la escritura
     * del espacio en memoria.
     * 
     * @param _scale escala en la que se desea escribir.
     */
    void setScale(string _scale);


    /**
     * @brief Coloca un punto de medición de memoria. 
     * 
     */
    void putMemPoint();

    /**
     * @brief Coloca el punto 0 de medición de memoria para
     * hallar un delta.
     */
    void putMemPoint_0();


    /**
     * @brief Coloca el punto 1 de medición de memoria para
     * hallar un delta. 
     */
    void putMemPoint_1();


    /**
     * @brief Retorna el espacio en memoria donde en el punto donde
     * se colocó el memPoint.
     * T -> total
     * D -> dinámica
     * S -> estática
     * 
     * @param _typeMem tipo de memoria a consultar, [T,D,S].
     * @return double espacio en memoria.
     */
    double getMem(string _typeMem);


    /**
     * @brief Retorna el delta en el espacio en memoria  entre los memPoint
     * 0 y 1.
     * T -> total
     * D -> dinámica
     * S -> estática
     * 
     * @param _typeMem tipo de memoria a consultar, [T,D,S].
     * @return double espacio en memoria.
     */
    double getDeltaMem(string _typeMem);


    /**
     * @brief Escribe en un fichero el estado de la memoria donde se colocó
     * el memPoint.
     * T -> total
     * D -> dinámica
     * S -> estática
     * 
     * @param _route ruta donde se desea escribir 
     * @param _typeMem tipo de memoria a escribir, [T,D,S].
     */
    void writeMem(string _route,string _typeMem);


    /**
     * @brief Escribe en un fichero el delta en el estado de la memoria 
     * entre los memPoint 0 y 1.
     * el memPoint.
     * T -> total
     * D -> dinámica
     * S -> estática
     * 
     * @param _route ruta donde se desea escribir 
     * @param _typeMem tipo de memoria a escribir, [T,D,S].
     */
    void writeDeltaMem(string _route,string _typeMem);
};

#endif // SPACELIB__H
