#include <Cmult.h>
#include <iostream>

using namespace cmult;

template <typename Dat>
Matrix<Dat>::Matrix(u_int row_size, u_int col_size) {
    this->row_size  = row_size;
    this->col_size  = col_size;
    this->data      = new Dat*[row_size];

    for (int i = 0; i < row_size; i++) 
    {
        this->data[i] = new Dat[col_size];
    }
}

template <typename Dat>
Matrix<Dat>::Matrix(const Matrix &A){
    this->row_size  = A.row_size;
    this->col_size  = A.col_size;
    for(u_int i = 0; i < this->row_size; i++)
    {
        for (u_int j = 0; j < this->col_size; j++)
        {
            this->data[i][j] = A.data[i][j];
        }
    }
}

template <typename Dat>
Matrix<Dat>::~Matrix(){
    for (u_int i = 0; i < this->row_size; i++)
        delete[] this->data[i];
    delete[] this->data;
}

template <typename Dat>
u_int Matrix<Dat>::getRowSize(){
    return this->row_size;
}

template <typename Dat>
u_int Matrix<Dat>::getColSize(){
    return this->col_size;
}


template <typename Dat>
Dat** Matrix<Dat>::getData(){
    return this->data;
}


template <typename Dat>
void Matrix<Dat>::print(){
    for (u_int i = 0; i < this->row_size; i++)
    {
        for (u_int j = 0; j < this->col_size; j++)
        {
            std::cout << this->data[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


template <typename Dat>
Matrix<Dat> cmult::mult(Matrix<Dat> &A, Matrix<Dat> &B){
    Matrix<Dat> result(A.getRowSize(), B.getColSize());
    Dat ** data_A      = A.getData();
    Dat ** data_B      = B.getData();
    Dat ** data_result = result.getData();

    for (u_int i = 0; i <  result.getRowSize(); i++)
    {    
        for (u_int j = 0; j < result.getColSize(); j++)
        {
            u_int fsum = 0;  
            for (u_int k = 0; k < result.getColSize(); k++)
            {
                fsum += data_A[i][k] * data_B[k][j];
            }
            data_result[i][j] = fsum;
        }
    }
    return result;
}


template <typename Dat>
Matrix<Dat> cmult::random_matrix(u_int row_size, u_int col_size){
    Matrix<Dat> A(row_size, col_size);
    Dat** data = A.getData();
    srand(time(NULL));
    for (size_t i = 0; i < A.getRowSize(); i++)
    {
        for (size_t j = 0; j < A.getColSize(); j++)
        {
            data[i][j] = (Dat) rand()%10;
        }    
    }
    return A;
}
